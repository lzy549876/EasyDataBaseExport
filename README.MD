<div align="center">
   <img alt="EasyDataBaseExport" src="image/logo.png">
   <a href="https://www.likethewind.top">
      <h1 align="center" style="color:#4da7fd">
         <b>EasyDataBaseExport</b>
      </h1>
   </a>
</div>

<p align="center">
    <img alt="IntelliJ IDEA 2023" src="https://img.shields.io/badge/IntelliJ IDEA-2023-blue.svg">
    <img alt="java" src="https://img.shields.io/badge/language-java-brightgreen.svg">
    <img src="https://img.shields.io/badge/JDK-1.8.0_151+-yellow.svg" alt="JDK Version">
    <img alt="release" src="https://img.shields.io/badge/release-0.1.7-green">
    <img alt="license" src="https://img.shields.io/badge/license-MIT-blue.svg">
</p> 

## 简介

该工具主要用于导出数据库表结构，格式为excel、word、markdown等，方便快速生成《数据库结构文档》，同时可以快速查看表的结构和相关信息  
该工具永久免费，欢迎大家体验！

### 📖 使用手册

[《EasyDataBaseExport说明文档》](https://www.likethewind.top/)

### 💬 运行环境要求

* 1.8.0_151+ （建议最好1.8.0_151以上的版本，因为151版本为JVM启用无限制强度管辖策略）

Windows版 jre8下载  
64位 -> x64  
32位 -> i586  
https://wwri.lanzouo.com/b02e9brti 密码:gpkp

### 🔍 数据库支持

- &#10004; MySQL
- &#10004; Oracle
- &#10004; SqlServer
- &#10004; PostgreSQL
- &#10004; 达梦
- &#10004; 人大金仓
- &#10004; DB2
- &#10004; 虚谷 

### 📑 文档支持

- &#10004; Word
- &#10004; Excel
- &#10004; Markdown 
- &#10004; Html 
- &#10004; Pdf 

### 👀 功能速览

> 注：图中涉及资源均来源于网络，仅用于展示工具功能，版权归其作者所有。

#### 连接

![在这里插入图片描述](image/login.jpg)

+ 【操作】-【编辑连接】

![在这里插入图片描述](image/edit.png)

+ 【操作】-【导入连接】

导入连接操作详见：[导入连接步骤](https://blog.csdn.net/kkk123445/article/details/122514124?spm=1001.2014.3001.5502#t5)

点击选择文件，选择上一步导出来的ncx文件，确定后即导入成功

![在这里插入图片描述](image/upload.png)

#### 主页

![在这里插入图片描述](image/home.jpg)

+ 导出自定义  

位置：【导出】-【配置...】
> `Word`、`Markdown`、`Html`和`Pdf`可以导出表的 `主键`和`索引`  
> `Excel`可以导出分 `单sheet`和`多sheet`  
> `默认导出路径`设置后，导出时，文件选择器会自动定位到设置的路径  
> `索引表头`可动态化展示，勾选即导出；反之，不导出    
> `字段表头`同`索引表头`  

配置位置如下：

![config.png](image/config.png)

点击右键（只有在勾选框选中时，才能右键），可以修改【名称】和【字段映射】

![点击右键](image/config-img.png)

点击确定

![点击右键](image/config-change.png)

点击保存，提示保存成功（再导出文档时，一切都改变了！）

![img.png](image/config-save.png)

导出结果（实现了真正意义上的自定义表头，快来制作适合自己的文档吧！）

![img.png](image/config-result.png)

#### 导出效果

+ Word（Microsoft Office提示更新域，点击确定后目录自动生成；WPS需手动建立目录）
  
![在这里插入图片描述](image/Word.png)

+ Excel

![在这里插入图片描述](image/Excel.png)

多sheet

![在这里插入图片描述](image/Excel_sheet.png)

+ Markdown

![在这里插入图片描述](image/Markdown.png)

+ Html

![在这里插入图片描述](image/Html.png)

+ Pdf
  
![在这里插入图片描述](image/Pdf.png)

### ☕ 项目结构

```md
EasyDataBaseExport
└─ src/main/java/com/easydatabaseexport
    ├── `common`    公共信息  
    ├── `core`      核心包，包含核心数据源和接口
    ├── `database`  所有数据库的数据源和实现
    ├── `entities`  实体
    ├── `enums`     枚举
    ├── `exception` 全局异常
    ├── `factory`   工厂
    ├── `log`       全局异常拦截日志
    ├── `navicat`   navicat处理
    ├── `ui`        界面
    └── `util`      工具
```

### 🏃 运行方式

* 1.双击
* 2.java -jar *.jar
* 3.运行`EasyDataBaseExportMain`的main方法

### 📦 编译打包

![img.png](image/maven.png)  
双击运行`assembly:assembly`进行打包

注：由于采用的部分驱动未上传maven仓库  
若打包报错，提示找不到包，请先运行 `mvn install`  
![img.png](image/install.png)

### ⏳ 下载

下载地址：右侧【发行版】中下载最新版   
其他历史版本：[https://wwr.lanzoui.com/b02c7c0cb](https://wwr.lanzoui.com/b02c7c0cb) 密码:1111  
github地址：[https://github.com/Zhuoyuan1/EasyDataBaseExport](https://github.com/Zhuoyuan1/EasyDataBaseExport)

### 📧 联系方式

QQ: 963565242       
Mail: 963565242@qq.com

### 📌 插件

本插件适用于多个Jetbrains平台（包括但不限于IntelliJ IDEA、PyCharm），基于Database Tool开发，所有Database Tool支持的数据库都是支持的。  
它实现了本工具的核心功能，欢迎试用！💖  
支持自定义表头，配置项位置 File -> Settings -> Other Settings -> EasyDatabaseExport
![输入图片说明](image/image.png)
插件下载地址    
<a href="https://plugins.jetbrains.com/plugin/19672-easy-database-export" target="_blank">
    ![plugins.svg](image/plugins.svg) 
</a>
> 欢迎下载使用，开发不易，希望多多支持，帮忙点star，谢谢各位！！！